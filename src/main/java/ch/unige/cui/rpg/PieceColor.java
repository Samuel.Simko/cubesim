package ch.unige.cui.rpg;

public enum PieceColor {
    WBO, WOG, WGR, WRB, YOB, YGO, YRG, YBR
}