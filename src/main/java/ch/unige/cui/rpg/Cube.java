package ch.unige.cui.rpg;

public interface Cube {
   public void scramble();
   public void move(Move move, int count);
   public String toString();
   // public Boolean isSolved();
   public String faces();
   public String coloredFaces();
}
