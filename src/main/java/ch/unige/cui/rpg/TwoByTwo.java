package ch.unige.cui.rpg;

public class TwoByTwo implements Cube {

    private Piece UBL;
    private Piece ULF;
    private Piece UFR;
    private Piece URB;

    private Piece DBL;
    private Piece DLF;
    private Piece DFR;
    private Piece DRB;

    public TwoByTwo(){
        this.UBL = new Piece(PieceColor.WBO, Orientation.FIRST);
        this.ULF = new Piece(PieceColor.WOG, Orientation.FIRST);
        this.UFR = new Piece(PieceColor.WGR, Orientation.FIRST);
        this.URB = new Piece(PieceColor.WRB, Orientation.FIRST);

        this.DBL = new Piece(PieceColor.YOB, Orientation.FIRST);
        this.DLF = new Piece(PieceColor.YGO, Orientation.FIRST);
        this.DFR = new Piece(PieceColor.YRG, Orientation.FIRST);
        this.DRB = new Piece(PieceColor.YBR, Orientation.FIRST);
    }

    public void scramble(){

    }

    public void move(Move move, int count){
        if (count == 0){
            return;
        } 
        switch (move){
            case U:
                Piece tmp = UBL.copy();
                UBL = ULF.copy();
                ULF = UFR.copy();
                UFR = URB.copy();
                URB = tmp;
                break;

            case B:
                Piece tmp1 = UBL.copy();
                Piece tmp2 = URB.copy();
                Piece tmp3 = DBL.copy();
                Piece tmp4 = DRB.copy();

                UBL = tmp2;
                URB = tmp4;
                DBL = tmp1;
                DRB = tmp3;

                UBL.orientPrev();
                URB.orientNext();
                DBL.orientNext();
                DRB.orientPrev();
                break;

            case L:
                tmp1 = UBL.copy();
                tmp2 = ULF.copy();
                tmp3 = DBL.copy();
                tmp4 = DLF.copy();

                UBL = tmp3;
                ULF = tmp1;
                DBL = tmp4;
                DLF = tmp2;

                UBL.orientPrev();
                ULF.orientNext();
                DBL.orientNext();
                DLF.orientPrev();
                break;

            case F:
                tmp1 = ULF.copy();
                tmp2 = UFR.copy();
                tmp3 = DLF.copy();
                tmp4 = DFR.copy();

                ULF = tmp3;
                UFR = tmp1;
                DLF = tmp4;
                DFR = tmp2;

                ULF.orientPrev();
                UFR.orientNext();
                DLF.orientNext();
                DFR.orientPrev();
                break;

            case R:
                tmp1 = UFR.copy();
                tmp2 = URB.copy();
                tmp3 = DFR.copy();
                tmp4 = DRB.copy();

                UFR = tmp3;
                URB = tmp1;
                DFR = tmp4;
                DRB = tmp2;

                UFR.orientPrev();
                URB.orientNext();
                DFR.orientNext();
                DRB.orientPrev();
                break;

            case D:
                tmp1 = DBL.copy();
                tmp2 = DLF.copy();
                tmp3 = DFR.copy();
                tmp4 = DRB.copy();

                DBL = tmp4;
                DLF = tmp1;
                DFR = tmp2;
                DRB = tmp3;
                break;
        }
        move(move, count - 1);
    }

    public String toString(){
        return UBL.print() + ULF.print() + UFR.print() + URB.print() + DBL.print() + DLF.print() + DFR.print()
                + DRB.print();
    }


    public String faces(){
        // StringBuilder sbuf = new StringBuilder();
        // Formatter fmt = new Formatter(sbuf);
        return String.format("    %c%c\n    %c%c\n %c%c %c%c %c%c %c%c\n %c%c %c%c %c%c %c%c\n    %c%c\n    %c%c\n",
             UBL.getChar(0),
             URB.getChar(0),
             ULF.getChar(0),
             UFR.getChar(0),

             UBL.getChar(2),
             ULF.getChar(1),
             ULF.getChar(2),
             UFR.getChar(1),
             UFR.getChar(2),
             URB.getChar(1),
             URB.getChar(2),
             UBL.getChar(1),


             DBL.getChar(1),
             DLF.getChar(2),
             DLF.getChar(1),
             DFR.getChar(2),
             DFR.getChar(1),
             DRB.getChar(2),
             DRB.getChar(1),
             DBL.getChar(2),

             DBL.getChar(0),
             DRB.getChar(0),
             DLF.getChar(0),
             DFR.getChar(0)
        );
    }

    public String coloredFaces(){

        return String.format("    %s%s\n    %s%s\n %s%s %s%s %s%s %s%s\n %s%s %s%s %s%s %s%s\n    %s%s\n    %s%s\n",
             UBL.getColoredChar(0),
             URB.getColoredChar(0),
             ULF.getColoredChar(0),
             UFR.getColoredChar(0),

             UBL.getColoredChar(2),
             ULF.getColoredChar(1),
             ULF.getColoredChar(2),
             UFR.getColoredChar(1),
             UFR.getColoredChar(2),
             URB.getColoredChar(1),
             URB.getColoredChar(2),
             UBL.getColoredChar(1),


             DBL.getColoredChar(1),
             DLF.getColoredChar(2),
             DLF.getColoredChar(1),
             DFR.getColoredChar(2),
             DFR.getColoredChar(1),
             DRB.getColoredChar(2),
             DRB.getColoredChar(1),
             DBL.getColoredChar(2),

             DBL.getColoredChar(0),
             DRB.getColoredChar(0),
             DLF.getColoredChar(0),
             DFR.getColoredChar(0)
        );
    }

}
