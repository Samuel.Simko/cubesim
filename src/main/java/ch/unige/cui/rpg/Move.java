package ch.unige.cui.rpg;

public enum Move {
    U, B, L, F, R, D
}
