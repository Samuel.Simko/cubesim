package ch.unige.cui.rpg;

public enum Orientation {
    FIRST, SECOND, THIRD;

    private static Orientation[] values = values();

    public Orientation prev() {
        return values[(ordinal() + 2) % values.length];
    }

    public Orientation next() {
        return values[(ordinal() + 1) % values.length];
    }
}