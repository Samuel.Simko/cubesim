package ch.unige.cui.rpg;

public class Piece {

    private PieceColor color;
    private Orientation orientation;
    
    public Piece(PieceColor color, Orientation orientation){
        this.color = color;
        this.orientation = orientation;
    }
    public PieceColor getColor(){
        return color;
    }

    public Orientation getOrientation(){
        return orientation;
    }

    public void orientNext(){
        orientation = orientation.next();
    }

    public void orientPrev(){
        orientation = orientation.prev();
    }

    public Piece copy(){
        return new Piece(color, orientation);
    }

    public String print(){
        return color.toString() + ", " + orientation.toString() + "\n";
    }

    public Character getChar(int i){
        return color.toString().charAt((orientation.ordinal() + i) % 3);
    }

    public String getColoredChar(int i){
        Character clr = color.toString().charAt((orientation.ordinal() + i) % 3);
        String RESET = "\u001B[40m";
        String ORANGE = "\033[48:5:208m";
        String BLACK = "\u001B[40m";
        String RED = "\u001B[41m";
        String GREEN = "\u001B[42m";
        String YELLOW = "\u001B[43m";
        String BLUE = "\u001B[44m";
        String PURPLE = "\u001B[45m";
        String CYAN = "\u001B[46m";
        String WHITE = "\u001B[47m";

        switch (clr){
            case 'W':
                return WHITE + " " + RESET;
            case 'O':
                return ORANGE + " "  + RESET;
            case 'G':
                return GREEN + " " + RESET;
            case 'R':
                return RED + " "  + RESET;
            case 'B':
                return BLUE + " "  + RESET;
            case 'Y':
                return YELLOW + " "  + RESET;
        }

        return "" + clr;
    }
}


