package ch.unige.cui.game;
import ch.unige.cui.rpg.*;


public class Game{
	public static void main(String[] args){

		TwoByTwo cube = new TwoByTwo();
		cube.move(Move.R, 1);
		cube.move(Move.F, 1);
		cube.move(Move.R, 1);
		cube.move(Move.F, 1);
		cube.move(Move.R, 1);
		cube.move(Move.F, 1);
		cube.move(Move.R, 1);
		System.out.println(cube.coloredFaces());

	}
}